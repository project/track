
function MapClass()
{
 var map;
 var baseIcon;
 var lasttrace;
 
// var debug = true;
// var debugid = "info";

 var trackbackurl = "";
 
 this.geturlproperties = geturlproperties;
 this.itemclic = itemclic;
 this.processXMLnode = processXMLnode;
 this.processrequest = processrequest;
 this.modified = modified;
 this.moved = moved;
 this.zoomed = zoomed;
 this.initmap = initmap;
 this.addMarker = addMarker;
 this.processxmlDoc = processxmlDoc;
 this.loadPoint = loadPoint;

 function addMarker(xmlnode) {
  	var point = loadPoint(xmlnode); 
   	var markee;
  	var icon = xmlnode.getAttribute("icon");
     	if (icon != "") {
         var anicon = new GIcon(baseIcon);
         anicon.image = icon;
     	 markee = new GMarker(point, anicon);
     	}
     	else
     	{      	
     	 markee = new GMarker(point);
     	}
        GEvent.addListener(markee, "click", function() {
           markee.openInfoWindowHtml(xmlnode.getAttribute("info"));
           itemclic(xmlnode.getAttribute("id"), parseFloat(xmlnode.getAttribute("x")),parseFloat(xmlnode.getAttribute("y")));
         });
        map.addOverlay(markee);
 }

 function geturlproperties() {
	 var bounds = map.getBounds();
         var southWest = bounds.getSouthWest();
         var northEast = bounds.getNorthEast();
	 var zl = 17 - map.getZoom();
	 var res = 'maxX='+northEast.lng()+'&maxY='+northEast.lat()+'&minY='+southWest.lat()+'&minX='+southWest.lng();
	 res = res + '&zoomlevel='+zl;
	 return res;
 }

 function itemclic(id, x, y) {
     var xmlfile = trackbackurl+'?action=clicked'+
      '&id='+id+
      '&xclicked=' + x +
      '&yclicked=' + y +
      '&'+geturlproperties();
     processrequest(xmlfile, true);
 }

 function loadPoint(xmlnode) {
     return new GLatLng(
      parseFloat(xmlnode.getAttribute("y")),
      parseFloat(xmlnode.getAttribute("x"))); 
 }

 function processXMLnode(marker) {

     if (marker.getAttribute("type") == "alert") {
   //     alert(marker.getAttribute("alert"));
     }
     if (marker.getAttribute("type") == "point") {
     	addMarker(marker);
     };
     if (marker.getAttribute("type") == "trace") {
         var pointspolyline = [];
         var markers = marker.getElementsByTagName("point");
         for (var i = 0; i < markers.length; i++) {
           pointspolyline.push(loadPoint(markers[i]));
         }
         var trace = new GPolyline(pointspolyline,
          marker.getAttribute("color"),
          parseFloat(marker.getAttribute("weight")),
          parseFloat(marker.getAttribute("opacity"))
         );
         map.addOverlay(trace);
         lasttrace = trace;
     }
     if (marker.getAttribute("type") == "clear") {
         map.clearOverlays();
     }
     if (marker.getAttribute("type") == "clearlasttrace") {
         if (lasttrace != null) {
            map.removeOverlay(lasttrace);
         }
     }
     if (marker.getAttribute("type") == "move") {
         map.setCenter(new GLatLng(parseFloat(marker.getAttribute("y")), parseFloat(marker.getAttribute("x"))), 17 - parseFloat(marker.getAttribute("zl")));
     }
     if (marker.getAttribute("type") == "info") {
         document.getElementById(marker.getAttribute("id")).innerHTML = marker.getAttribute("text");
     }
 }

 function processxmlDoc(xmlDoc) {
      if (xmlDoc.documentElement != null) {
       var markers = xmlDoc.documentElement.getElementsByTagName("data");
       markerstoadd = [];

       //alert("ok!");

       for (var i = 0; i < markers.length; i++) { // er"<"erg<>
         processXMLnode(markers[i]);
       }
      }
 }

 function processrequest(url, async) {
     var request = GXmlHttp.create();

     request.open("GET", url, async);

     if (async) {
      request.onreadystatechange = function() {
        if (request.readyState == 4) {
//          alert(request.responseText);
          processxmlDoc(request.responseXML);
        }
      }
      request.send(null);
     }
     else
     {
      request.send(null);
//      alert(request.responseText);
      processxmlDoc(request.responseXML);
     }       
 }

 function modified(actionmade, async) {
    var xmlfile = trackbackurl+'?action='+actionmade+'&'+geturlproperties();
    processrequest(xmlfile, async);
 }

 function moved() {
	modified('moved', true);
 }

 function zoomed(oldZoomLevel, newZoomLevel) {
	modified('zoomed', true);
 }

 function initmap(trackback, divid) {

  trackbackurl = trackback;

//  alert("DBG: TrackBack: "+trackback+" DivId: "+divid);
//  alert(document.getElementById(divid));

  map = new GMap2(document.getElementById(divid));

  baseIcon = new GIcon();
  baseIcon.shadow = "http://www.google.com/mapfiles/shadow50.png";
  baseIcon.iconSize = new GSize(20, 34);
  baseIcon.shadowSize = new GSize(37, 34);
  baseIcon.iconAnchor = new GPoint(9, 34);
  baseIcon.infoWindowAnchor = new GPoint(9, 2);
  baseIcon.infoShadowAnchor = new GPoint(18, 25);

  map.addControl(new GLargeMapControl());
  map.addControl(new GMapTypeControl());

  GEvent.addListener(map, 'moveend', moved);
  GEvent.addListener(map, 'zoom', zoomed);

  map.addMapType(G_PHYSICAL_MAP);
//  map.addMapType(G_SATELLITE_3D_MAP);

/*
  var tileIGN = new GTileLayer(new GCopyrightCollection(""),12,15);
  tileIGN.getTileUrl = function(a,b) {
        var z = 17 - b;
        return "http://gabriel.landais.org/maps/map.php?zoom="+z+"&x="+a.x+"&y="+a.y;
  }
  tileIGN.isPng = function() {return true;}

  var layerIGN = [tileIGN];
  var mapIGN = new GMapType(layerIGN, G_SATELLITE_MAP.getProjection(), "IGN", G_SATELLITE_MAP);
  map.addMapType(mapIGN);
*/

  map.setCenter(new GLatLng(0, 0), 0);
//  map.setMapType(G_SATELLITE_TYPE);
//  map.setMapType(G_HYBRID_MAP);
//  map.setMapType(G_NORMAL_MAP);
  map.setMapType(G_PHYSICAL_MAP);
//  map.enableScrollWheelZoom();
  map.enableContinuousZoom();

  modified('initsync', false);
  modified('initasync', true);

 }

}
