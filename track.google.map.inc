<?
/*
 * Created on 4 d�c. 2005
 */
function htmlentities2unicodeentities($input) {
	$htmlEntities = array_values(get_html_translation_table(HTML_ENTITIES, ENT_QUOTES));
	$entitiesDecoded = array_keys(get_html_translation_table(HTML_ENTITIES, ENT_QUOTES));
	$num = count($entitiesDecoded);
	for ($u = 0; $u < $num; $u ++) {
		$utf8Entities[$u] = '&#'.ord($entitiesDecoded[$u]).';';
	}
	return str_replace($htmlEntities, $utf8Entities, htmlentities(utf8_decode($input)));
}

class GoogleMap {
	var $dataarray = array ();

	var $Xmin;
	var $Xmax;
	var $Ymin;
	var $Ymax;
	var $zlevel;

	var $xclicked;
	var $yclicked;
	var $idclicked;

	var $mapaction;

	function echoXMLPoint($pointtag, $point) {
		echo "<".$pointtag." x=\"".$point[0]."\" ";
		echo "y=\"".$point[1]."\" ";
		echo "/>";
	}

	function Init() {
		$this->Xmin = $_GET['minX'];
		$this->Xmax = $_GET['maxX'];
		$this->Ymin = $_GET['minY'];
		$this->Ymax = $_GET['maxY'];
		$this->zlevel = $_GET['zoomlevel'];
		$this->mapaction = $_GET['action'];
		if ($this->mapaction == 'clicked') {
			$this->xclicked = $_GET['xclicked'];
			$this->yclicked = $_GET['yclicked'];
			$this->idclicked = $_GET['id'];
		}
	}

	function AddPoint($x, $y, $id, $info, $icon = "") {
		$index = count($this->dataarray);
		$this->dataarray[$index][0] = "point";
		$this->dataarray[$index][1] = $x;
		$this->dataarray[$index][2] = $y;
		$this->dataarray[$index][3] = $id;
		$this->dataarray[$index][4] = $info;
		$this->dataarray[$index][5] = $icon;
	}

	function AddTrace($points, $color = "#ff0000", $weight = 1, $opacity = 0.7) {
		$index = count($this->dataarray);
		$this->dataarray[$index][0] = "trace";
		$this->dataarray[$index][1] = $points;
		$this->dataarray[$index][2] = $color;
		$this->dataarray[$index][3] = $weight;
		$this->dataarray[$index][4] = $opacity;
	}

	function AddClear() {
		$index = count($this->dataarray);
		$this->dataarray[$index][0] = "clear";
	}

	function AddClearLastTrace() {
		$index = count($this->dataarray);
		$this->dataarray[$index][0] = "clearlasttrace";
	}

	function MoveAndZoom($x, $y, $zl) {
		$index = count($this->dataarray);
		$this->dataarray[$index][0] = "move";
		$this->dataarray[$index][1] = $x;
		$this->dataarray[$index][2] = $y;
		$this->dataarray[$index][3] = $zl;
	}

	function DisplayInfo($id, $info) {
		$index = count($this->dataarray);
		$this->dataarray[$index][0] = "info";
		$this->dataarray[$index][1] = $id;
		$this->dataarray[$index][2] = $info;
	}

	function DisplayAlert($alert) {
		$index = count($this->dataarray);
		$this->dataarray[$index][0] = "alert";
		$this->dataarray[$index][1] = $alert;
	}

	function ShowXMLforAJAX() {
		header("content-type:text/xml");
//		echo "<xmldata>\n";
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<xmldata>\n";

//echo "Count: ".count($this->dataarray);
		for ($i = 0; $i < count($this->dataarray); $i ++) {

			if ($this->dataarray[$i][0] == "clear") {
				echo "<data type=\"clear\"></data>";
			}
			if ($this->dataarray[$i][0] == "clearlasttrace") {
				echo "<data type=\"clearlasttrace\"></data>";
			}
			if ($this->dataarray[$i][0] == "point") {
				echo "<data type=\"point\" ";
				echo "x=\"".$this->dataarray[$i][1]."\" ";
				echo "y=\"".$this->dataarray[$i][2]."\" ";
				echo "id=\"".$this->dataarray[$i][3]."\" ";
				echo "info=\"".htmlentities2unicodeentities($this->dataarray[$i][4])."\" ";
				echo "icon=\"".$this->dataarray[$i][5]."\" ";
				echo "></data>\n";
			}

			if ($this->dataarray[$i][0] == "move") {
				echo "<data type=\"move\" ";
				echo "x=\"".$this->dataarray[$i][1]."\" ";
				echo "y=\"".$this->dataarray[$i][2]."\" ";
				echo "zl=\"".$this->dataarray[$i][3]."\" ";
				echo "></data>\n";
			}
			if ($this->dataarray[$i][0] == "info") {
				echo "<data type=\"info\" ";
				echo "id=\"".$this->dataarray[$i][1]."\" ";
				echo "text=\"".htmlentities2unicodeentities($this->dataarray[$i][2])."\" ";
				echo "></data>\n";
			}
			if ($this->dataarray[$i][0] == "alert") {
				echo "<data type=\"alert\" ";
				echo "alert=\"".htmlentities2unicodeentities($this->dataarray[$i][1])."\" ";
				echo "></data>\n";
			}
			if ($this->dataarray[$i][0] == "trace") {
				echo "<data type=\"trace\" ";
				echo "color=\"".$this->dataarray[$i][2]."\" ";
				echo "weight=\"".$this->dataarray[$i][3]."\" ";
				echo "opacity=\"".$this->dataarray[$i][4]."\" ";
				echo ">";
				for ($j = 0; $j < count($this->dataarray[$i][1]); $j ++) {
					$this->echoXMLPoint("point", $this->dataarray[$i][1][$j]);
				}
				echo "</data>\n";
			}

		}
		echo "</xmldata>";
	}

}
