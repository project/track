<?

class GPX_file {
  var $coordonnees = array();
  var $catData = "";
  var $catDataTime = "";
  var $currentTag = "";
  var $count = 0;
  var $ZMax = -1;
  var $GPXfile = "";

  function debutElement($parser, $name, $attrs)
  {
     $this->currentTag = $name;

     if ($name == "TRKPT") {
       $this->coordonnees[$this->count][0] = $attrs["LAT"];
       $this->coordonnees[$this->count][1] = $attrs["LON"];
       $this->count++;
     }
  }

  function characterData($parser, $data)
  {
        if ($this->currentTag == "ELE") {
          $this->catData .= trim($data);
        }
        if ($this->currentTag == "TIME") {
          $this->catDataTime .= trim($data);
        }
  }

  function getZMax() {
   return $ZMax;
  }

  function finElement($parser, $name)
  {
  	if ($name = "ELE") {
  	if ($this->catData != "") {
          $ZMax = max($ZMax, $this->catData); 
          $this->coordonnees[$this->count-1][2] = $this->catData;
        }
  	}

  	if ($name = "TIME") {
  	if ($this->catDataTime != "") {
          $this->coordonnees[$this->count-1][3] = $this->catDataTime;
        }
  	}

  	unset($this->catData);
  	$this->catData = "";
  	unset($this->catDataTime);
  	$this->catDataTime = "";
  }

  function DenivelePositif() {
    $denivp = 0;
    
    $z = $this->ZArray();

    for ($i=1; $i<count($z); $i++) {
     if (($z[$i] - $z[$i-1])>0)
      {
       $denivp += ($z[$i] - $z[$i-1]);
      }
    }
    return $denivp;
  }

  function DeniveleNegatif() {
    $denivm = 0;
    $z = $this->ZArray();

    for ($i=1; $i<count($z); $i++) {
     if (($z[$i] - $z[$i-1])<0)
      $denivm += ($z[$i] - $z[$i-1]);
    }
    return $denivm;
  }
              
  function XArray() {
    $res = array();

    $r = 6378;
    
    $pi = atan2(1,1) * 4;
    
    $dist = 0;
    $res[] = 0;
    for ($i=1; $i<$this->count; $i++) {
     $a1 = $this->coordonnees[$i-1][0] * ($pi/180);
     $a2 = $this->coordonnees[$i][0] * ($pi/180);
     $b1 = $this->coordonnees[$i-1][1] * ($pi/180);
     $b2 = $this->coordonnees[$i][1] * ($pi/180);
     
     if ( ($a1==$a2) && ($b1==$b2) )
      $delta = 0;
     else
      {
       $toacos = cos($a1)*cos($b1)*cos($a2)*cos($b2) + cos($a1)*sin($b1)*cos($a2)*sin($b2) + sin($a1)*sin($a2);
    
       if ($toacos > 1)
        $delta = 0;
       else
        $delta = acos($toacos) * $r;
      }
     $dist += $delta;
     $res[] = $dist;
    }
    return $res;
  }

  function Pentes() {
     $res = array();
     for ($i=0; $i<41; $i++) {
       $res[] = $i-20;
     }
     return $res;
  }

  function RepartitionPentes() {
     $datax = $this->XArray();
     $datay = $this->ZArray();

     $res = array();
     for ($i=0; $i<41; $i++) {
       $res[] = 0;
     }

     for ($i=1; $i<count($datax); $i++) {
       $distance = ($datax[$i] - $datax[$i - 1]) * 1000;
       $dz = $datay[$i] - $datay[$i - 1];
       if ($distance>0) {
        $pente = (100 * $dz) / $distance;
        if ($pente<-20) {
          $res[0] += $distance;
        } else
        if ($pente>20) {
          $res[40] += $distance;
        } else
        {
           $ires = round($pente + 20);
           $res[$ires] += $distance;
        }
       }
     }

     for ($i=0; $i<41; $i++) {
       $res[$i] = $res[$i] / 1000;
     }
     return $res;
  }

  function FilterValue($valuearray) {
    sort($valuearray, SORT_NUMERIC);
    $newarray = array();
    for ($i=1; $i<count($valuearray)-1; $i++) {
      $newarray[] = $valuearray[$i];
    }
    return array_sum($newarray)/count($newarray);
  }

  function FilterArray($anarray) {
    $res = array();

    if (count($anarray)>5) {
     for ($i=0; $i<5; $i++) {
      $res[] = $anarray[$i];
     }

     for ($i=5; $i<count($anarray); $i++) {
      $res[] = $this->FilterValue( array($anarray[$i-5], $anarray[$i-4], $anarray[$i-3], $anarray[$i-2], $anarray[$i-1], $anarray[$i]) );
     }
    }
    else
     $res = $anarray;

    return $res;
  }

  function ZArray() {
    $res = array();
    for ($i=0; $i<$this->count; $i++) {
     $res[] = $this->coordonnees[$i][2];
    }
    return $this->FilterArray($res);
  }

  function Longueur() {
    $r = 6378;
    
    $pi = atan2(1,1) * 4;
    
    $dist = 0;
    for ($i=1; $i<$this->count; $i++) {
     $a1 = $this->coordonnees[$i-1][0] * ($pi/180);
     $a2 = $this->coordonnees[$i][0] * ($pi/180);
     $b1 = $this->coordonnees[$i-1][1] * ($pi/180);
     $b2 = $this->coordonnees[$i][1] * ($pi/180);
     
     if ( ($a1==$a2) && ($b1==$b2) )
      $delta = 0;
     else
      {
       $toacos = cos($a1)*cos($b1)*cos($a2)*cos($b2) + cos($a1)*sin($b1)*cos($a2)*sin($b2) + sin($a1)*sin($a2);
    
       if ($toacos > 1)
        $delta = 0;
       else
        $delta = acos($toacos) * $r;
      }
     $dist += $delta;
    }
    return $dist;
  }

  function XMLforProfile() {
     $datax = $this->XArray();
     $datay = $this->ZArray();

     $n = count($datax);
     
     $ymin = 100000;
     $ymax = -100000;

     for ($i=0; $i<($n-1); $i++) {
       $ymin = min($ymin, $datay[$i]);
       $ymax = max($ymax, $datay[$i]);
     }

     echo "<graph caption='".t("Profile")."' xaxisname='".t("Distance")."' yaxisname='".t("Altitude")."' ";
     echo "showAreaBorder='0' lineThickness='1' numdivlines='10' numVDivLines='10' showgridbg='1' showhovercap='1' ";
     echo "showVerLines='0' hoverCapSepChar=' ' yaxisminvalue='".$ymin."' yaxismaxvalue='".$ymax."' anchorScale='0' showNames='1' showValues='0' animation='0'>\n";

     $ndata = 200;
     $step = ($datax[$n-1])/$ndata;
     
//     echo $step;

     $curindex = 0;

     $values = array();
     $xvalues = array();

     for ($i=0; $i<($ndata-1); $i++) {
       $xmin = $i * $step;
       $xmax = ($i+1) * $step;
       $index = $curindex;

//       echo $i." ".$xmin." ".$xmax." ".."\n";

       while (($datax[$index] < $xmax) && ($index<$n))
        $index++;
       $val = 0;
       $valn = 0;
       
       $maxv = -10000;
       $minv = 25000;

       for ($j=$curindex; $j<$index; $j++) {
         $val+=$datay[$j];
         $maxv = max($maxv, $datay[$j]);
         $minv = min($minv, $datay[$j]);
         $valn++;
       }
       if ($valn>0) {
        $vavg = $val / $valn;

        $deltamin = $vavg - $minv;
        $deltamax = $maxv - $vavg;

        $currentval = $vavg;

        if (max($deltamin, $deltamax)>15) {
          if ($deltamin>$deltamax)
            $currentval = $minv;
           else
            $currentval = $maxv;
        }

        $values[] = $currentval;
        $xvalues[] = $xmin;

        $curindex = $index;
       }
     }

     $values[] = $datay[$n-1];
     $xvalues[] = $datax[$n-1];

     for ($i=0; $i<count($values); $i++) {
      if (($i==0) || ($i==(count($values)-1)))
       echo "\n<set name='".sprintf('%2.2f',$xvalues[$i])."' value='".$values[$i]."' showName='1'/>";
      else
       echo "\n<set value='".$values[$i]."' showName='0'/>";
     }
     echo "</graph>";
  }


  function XMLforRepartition() {
/*
<graph bgcolor='e1f5ff' canvasbgcolor="FFFFFF" caption='Decline in Net Interest Margins of Asian Banks (1995-2001)' subCaption='(in Percentage %)' yaxismaxvalue="2" yaxisminvalue='-2.5' yaxisname='Points' xaxisname='Country'   gridbgcolor='f1f1f1' hovercapbg='FFFFDD' hovercapborder='000000' numdivlines='4' divlinecolor='999999' zeroPlaneColor="333333" zeroPlaneAlpha='40' numberSuffix=' %'>
  <set name='Taiwan' value='-0.33' color='F23456'/>
  <set name='Malaysia' value='-0.27' color='FF6600'/>
  <set name='Hong Kong' value='0.40' color='009966' alpha='70'/>
  <set name='Philippines' value='0.6'  color='CC3399'/>
  <set name='Singapore' value='-0.8'  color='FFCC33' alpha='70'/>
  <set name='Thailand' value='-2.2' color='F23456'/>
  <set name='India' value='1.2' color='FF6600'/>
<trendlines>
  <line value='0.8' displayValue='Good' color='FF0000' thickness='1'/>
  <line value='-0.4' displayValue='Bad' color='009999' thickness='1'/>
</trendlines>
</graph>
*/

     echo "<graph caption='".t("Histogram")."' subCaption='' yaxisname='".t("Distance")."' xaxisname='".t("Difficulty")."' ynumdivlines='4' numberSuffix=' km'>";

     $trans = get_html_translation_table(HTML_ENTITIES);

     $l = $this->Longueur();

     $datax = $this->Pentes();
     $datay = $this->RepartitionPentes();

     $tot = 0;
     for ($i=0; $i<10; $i++) {
        $tot += $datay[$i];
     }
     echo "<set name='".t("Steep descent")."' value='".$tot."' />";   //<-10%

     $tot = 0;
     for ($i=10; $i<19; $i++) {
        $tot += $datay[$i];
     }
     echo "<set name='".t("Descent")."' value='".$tot."' />"; //(entre -10% et -2%)

     $tot = 0;
     for ($i=19; $i<22; $i++) {
        $tot += $datay[$i];
     }
     echo "<set name='".t("Flat")."' value='".$tot."' />"; // (entre -2% et 2%)

     $tot = 0;
     for ($i=22; $i<31; $i++) {
        $tot += $datay[$i];
     }
     echo "<set name='".t("Ascent")."' value='".$tot."' />"; //(entre 2% et 10%)

     $tot = 0;
     for ($i=31; $i<41; $i++) {
        $tot += $datay[$i];
     }
     echo "<set name='".t("Steep ascent")."' value='".$tot."' />"; //(>10%)

     echo "</graph>";
  }

 function htmlentities2unicodeentities ($input) {
  $htmlEntities = array_values (get_html_translation_table (HTML_ENTITIES, ENT_QUOTES));
  $entitiesDecoded = array_keys  (get_html_translation_table (HTML_ENTITIES, ENT_QUOTES));
  $num = count ($entitiesDecoded);
  for ($u = 0; $u < $num; $u++) {
   $utf8Entities[$u] = '&#'.ord($entitiesDecoded[$u]).';';
  }
  return str_replace ($htmlEntities, $utf8Entities, $input);
 }

 function PointsForGoogleMaps($npoints) {
    $points = array();
    $step = max(1, floor(count($this->coordonnees) / $npoints));

    $j=0;
    for ($i=0; $i<$this->count; $i++) {
     if  ($i % $step < 1) {
      $points[$j][1] =  $this->coordonnees[$i][0];
      $points[$j][0] = $this->coordonnees[$i][1];
      $j++;
     }
    }

    return $points;
 }

 function ConvertToKML() {
  header('Content-Type: application/vnd.google-earth.kml+xml');
  header('Content-Disposition: attachment; filename="'.substr($this->GPXfile, 0, -4).'.kml"');
  echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
?>
<kml xmlns="http://earth.google.com/kml/2.1">
<Document>
  <name><? echo $this->GPXfile; ?></name>
  <open>1</open>
  <Style id="lineStyle">
	<LineStyle>
		<color>6422FF22</color>
		<width>4</width>
	</LineStyle>
  </Style>
  <Folder>
    <open>1</open>
    <name><? echo $this->GPXfile; ?></name>
    <Placemark>
      <visibility>1</visibility>
      <name><? echo $this->GPXfile; ?></name>
      <Style>
        <IconStyle>
          <Icon>
            <href>root://icons/palette-4.png</href>
            <y>32</y>
            <w>32</w>
            <h>32</h>
          </Icon>
        </IconStyle>
      </Style>
      <Point>
        <extrude>1</extrude>
        <coordinates><? echo $this->coordonnees[0][1].",".$this->coordonnees[0][0].",0"; ?></coordinates>
      </Point>
      <description><? echo $this->htmlentities2unicodeentities(t("Length")." : ". sprintf('%2.2f', $this->Longueur()) ."km &lt;br&gt; ".t("D+")." : ". sprintf('%2.0f', $this->DenivelePositif()) ."m / ".t("D-")." : ". sprintf('%2.0f', $this->DeniveleNegatif()) ."m"); ?></description>
    </Placemark>
    <Placemark>
	<name>Path</name>
	<styleUrl>#lineStyle</styleUrl>
	<LineString>
	<tessellate>1</tessellate>
           <coordinates>
<?
    for ($i=0; $i<count($this->coordonnees) - 1; $i++) {
?>
    	    <? echo $this->coordonnees[$i][1].",".$this->coordonnees[$i][0].",0"; ?>
<?
    }
?>
           </coordinates>
	</LineString>
     </Placemark>
  </Folder>
 </Document>
</kml>
<?
 }

 function ConvertToTRK() {

  header('Content-Disposition: attachment; filename="'.substr($this->GPXfile, 0, -4).'.trk"');
  ?>

H  SOFTWARE NAME & VERSION
I  PCX5 2.09

H  R DATUM                IDX DA             DF             DX             DY             DZ
M  G WGS 84               121 +0.000000e+000 +0.000000e+000 +0.000000e+000 +0.000000e+000 +0.000000e+000

H  COORDINATE SYSTEM
U  LAT LON DEG

H  LATITUDE    LONGITUDE    DATE      TIME     ALT    ;track
<?php
    for ($i=0; $i<$this->count; $i++) {
      $lat = $this->coordonnees[$i][0];
      $lon = $this->coordonnees[$i][1];
      $alt = abs($this->coordonnees[$i][2]);
      printf('T  ');
//T  N47.5118714 W002.2987803 19-OCT-03 08:46:25 00038

      if ($lat>0) {
        printf('N', $lat);
      }
      else
      {
      	$lat = - $lat;
        printf('S', $lat);
      }

      printf('%02d', $lat);
      printf('.');
      // 0.5118714 -> 5118714
      printf('%07d', ($lat - floor($lat)) * 10000000);

      printf(' ');

      if ($lon>0) {
        printf('E', $lon);
      }
      else
      {
      	$lon = - $lon;
        printf('W', $lon);
      }

      printf('%03d', $lon);
      printf('.');
      // 0.5118714 -> 5118714
      printf('%07d', ($lon - floor($lon)) * 10000000);

      $datetime = $this->coordonnees[$i][3];
      // 2005-10-29T11:00:49Z -> 30-DEC-99 00:00:00
      $annee = substr($datetime, 0, 4);
      $mois = substr($datetime, 5, 2);
      $jour = substr($datetime, 8, 2);
      $heure = substr($datetime, 11, 2);
      $minute = substr($datetime, 14, 2);
      $seconde = substr($datetime, 17, 2);
      $dt = mktime($heure, $minute, $seconde, $mois, $jour, $annee);
      printf(" ".strtoupper(date("d-M-y H:i:s", $dt))." ");
      printf('%05d', $alt);
      printf("\n");
    }
 }

 function getInfos(&$latstart,&$lonstart,&$latmin,&$latmax,&$lonmin,&$lonmax,&$length,&$dzminu,&$dzplus,&$zmin,&$zmax) {
   $length = $this->Longueur();
   $dzplus = $this->DenivelePositif();
   $dzminu = $this->DeniveleNegatif();

   $latmin = 200.0;
   $lonmin = 200.0;
   $zmin = 20000.0;

   $latmax = -200.0;
   $lonmax = -200.0;
   $zmax = -20000.0;

   $latstart = 0.0;
   $lonstart = 0.0;

    for ($i=0; $i<$this->count; $i++) {
      $lat = $this->coordonnees[$i][0];
      $lon = $this->coordonnees[$i][1];
      $alt = $this->coordonnees[$i][2];

      $latstart += $lat;
      $lonstart += $lon;

      $latmin = min($latmin, $lat);
      $lonmin = min($lonmin, $lon);
      $zmin = min($zmin, $alt);

      $latmax = max($latmax, $lat);
      $lonmax = max($lonmax, $lon);
      $zmax = max($zmax, $alt);
    }
    $latstart = (1.0*$latstart) / (1.0*$this->count);
    $lonstart = (1.0*$lonstart) / (1.0*$this->count);
 }

 function LoadFromFile($filename) {
    $this->GPXfile = basename($filename);
    $xml_parser = xml_parser_create();
    xml_set_element_handler($xml_parser, array(&$this,"debutElement"), array(&$this,"finElement"));
    xml_set_character_data_handler($xml_parser, array(&$this,"characterData"));
    if (!($fp = fopen($filename, "r"))) {
       die("Can't open file: ".$filename);
    }

    while ($data = fread($fp, 4096)) {
       if (!xml_parse($xml_parser, $data, feof($fp))) {
           die(sprintf("erreur XML : %s � la ligne %d",
                       xml_error_string(xml_get_error_code($xml_parser)),
                       xml_get_current_line_number($xml_parser)));
       }
    }
    xml_parser_free($xml_parser);
 }

}
